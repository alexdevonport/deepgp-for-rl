import numpy as np
import GPy
import deepgp
import scipy.optimize
from sklearn.preprocessing import MinMaxScaler
import math

import utils


class ModelBasedPolicy(object):

    def __init__(self,
                 env,
                 init_dataset,
                 horizon=15,
                 num_random_action_selection=16,
                 nn_layers=1,
                 exploration_factor=1):
        self.n_gp_layers=10
        #self._cost_fn = env.cost_fn
        self._state_dim = env.observation_space.shape[0]
        self._action_dim = env.action_space.shape[0]
        self._action_space_low = env.action_space.low
        self._action_space_high = env.action_space.high
        self.dataset = init_dataset
        self._horizon = horizon
        self._num_random_action_selection = num_random_action_selection
        self._nn_layers = nn_layers
        self.exploration_factor = exploration_factor

        # --------- Initial Model Construction ----------#



    def train(self):
        states_data = []
        actions_data = []
        delta_states_data = []
        rewards_data = []
        for states, actions, next_states, rewards, dones in self.dataset.rollout_iterator():
            for state, action, next_state, reward in zip(states, actions, next_states, rewards):
                states_data.append(state)
                actions_data.append(action)
                delta_state = next_state - state
                delta_states_data.append(delta_state)
                rewards_data.append([reward])
        state_action_pairs_data = np.concatenate((states_data,actions_data), axis=1)
        X_tr = np.array(state_action_pairs_data)
        Y_tr = np.array(delta_states_data)
        R_tr = np.array(rewards_data)


        # Number of latent dimensions (single hidden layer, since the top layer is observed)
        Q = 3
        # Define what kernels to use per layer
        kern1 = GPy.kern.RBF(Q, ARD=True)
        kern2 = GPy.kern.RBF(X_tr.shape[1], ARD=False)
        # don't know if I actually need to specify different kernels for different DGPs, but I'll be careful for now
        kern1_r = GPy.kern.RBF(Q, ARD=True)
        kern2_r = GPy.kern.RBF(X_tr.shape[1], ARD=False)

        # Number of inducing points to use
        num_inducing = 40
        # Whether to use back-constraint for variational posterior
        back_constraint = False
        # Dimensions of the MLP back-constraint if set to true

        # DGP model for state transition function
        m = deepgp.DeepGP([Y_tr.shape[1], Q, X_tr.shape[1]], Y_tr, X=X_tr, kernels=[kern1, kern2],
                                            num_inducing=num_inducing, back_constraint=back_constraint)

        # DGP model for reward function
        r = deepgp.DeepGP([1, Q, X_tr.shape[1]], R_tr, X=X_tr, kernels=[kern1_r, kern2_r],
                          num_inducing=num_inducing, back_constraint=back_constraint)

        print('Now training model for state transition function...')
        for i in range(len(m.layers)):
            output_var = m.layers[i].Y.var() if i == 0 else m.layers[i].Y.mean.var()
            m.layers[i].Gaussian_noise.variance = output_var * 0.01
            m.layers[i].Gaussian_noise.variance.fix()

        nopt1 = 25
        nopt2 = 50
        m.optimize(max_iters=nopt1, messages=True)
        # Unfix noise variance now that we have initialized the model
        for i in range(len(m.layers)):
            m.layers[i].Gaussian_noise.variance.unfix()
        m.optimize(max_iters=nopt2, messages=True)

        print('Now training model for reward function...')
        for i in range(len(r.layers)):
            output_var = r.layers[i].Y.var() if i == 0 else r.layers[i].Y.mean.var()
            r.layers[i].Gaussian_noise.variance = output_var * 0.01
            r.layers[i].Gaussian_noise.variance.fix()

        r.optimize(max_iters=nopt1, messages=True)
        # Unfix noise variance now that we have initialized the model
        for i in range(len(r.layers)):
            r.layers[i].Gaussian_noise.variance.unfix()
        r.optimize(max_iters=nopt2, messages=True)


        #m.optimize(max_iters=150, messages=True)
        self.dynamics_model = m
        self.reward_function_model = r

        return None


    def predict_state(self, state, action):
        """
        Predicts the next state given the current state and action

        returns:
            next_state_pred: predicted next state

        implementation detils:
            (i) The state and action arguments are 1-dimensional vectors (NO batch dimension)
        """
        m = self.dynamics_model

        state_n = state
        action_n = action

        if len(np.shape(state_n)) == 1:
            state_n = state_n[None]
        if len(np.shape(action_n)) == 1:
            action_n = action_n[None]
        #print(np.shape(state_n))
        #print(np.shape(action_n))
        state_action = np.concatenate((state_n, action_n),axis=1)
        #state_action = np.reshape(state_action, (-1,1))
        delta_pred_n = m.predict(state_action)[0]
        delta_pred_std_n = m.predict(state_action)[1]
        #print(np.shape(delta_pred_n))
        #delta_pred_n = np.reshape(delta_pred_n, (-1))
        delta_pred = delta_pred_n
        delta_pred_var = delta_pred_std_n
        #delta_pred = utils.unnormalize(delta_pred_n, delta_mean, delta_std)
        next_state_pred = state + delta_pred

        if len(np.shape(state_n)) == 1:
            next_state_pred = next_state_pred[0]
        #print(next_state_pred)
        return next_state_pred, delta_pred_var

    def predict_reward(self, state, action):
        """
        Uses the reward function DGP model to predict the value of the reward function at the
        given state and action.

        implementation question: presently, this function returns the mean and std of the reward. Would
        it be better to have it return the mean and entropy instead?
        :param state:
        :param action:
        :return:
        """
        if len(np.shape(state)) == 1:
            state = state[None]
        if len(np.shape(action)) == 1:
            action = action[None]
        r = self.reward_function_model
        state_action = np.concatenate((state, action),axis=1)
        rew_pred_mean, rew_var = r.predict(state_action)
        return rew_pred_mean, rew_var

    def add_datapoint_single(self, state, action, next_state, reward):
        state_action_pair = np.concatenate((state,action))
        delta = next_state - state
        #new_x_data = np.array(state_action_pair)
        new_x_data = state_action_pair[None]
        new_delta_data = np.array(delta)[None]
        new_reward_data = np.array(reward)[None]
        self.dynamics_model.append_XY(new_delta_data, new_x_data)
        self.reward_function_model.append_XY(new_reward_data, new_x_data)
        return None

    def get_action(self, state):
        """

        :param state:
        :return:
        """
        # TODO: let the user pass in an 'initial action', or make it be a class property, to speed up the mpc
        def cost_at_horizon(actions, initial_state):
            actions = np.reshape(actions, (self._horizon, self._action_dim))
            total_rewards = 0
            current_state = state
            next_state_min_var = self.dynamics_model.layers[0].Y.var()
            reward_min_var = self.reward_function_model.layers[0].Y.var()
            for t in range(self._horizon):
                current_action = actions[t]
                next_state, next_state_var = self.predict_state(current_state, current_action)
                reward, reward_var = self.predict_reward(current_state, current_action)
                next_state_information_gain = gaussian_entropy(next_state_var) - gaussian_entropy(next_state_min_var)
                reward_information_gain = gaussian_entropy(reward_var) - gaussian_entropy(reward_min_var)
                exploration_bonus = self.exploration_factor * (next_state_information_gain + reward_information_gain)
                total_rewards += reward + exploration_bonus
                # total_rewards is a sequence for some reason, fix that
                # probably due to the state/action variances being multidimensional.
            return -total_rewards

        initial_actions = np.zeros((self._horizon, self._action_dim))
        for i in range(self._horizon):
            initial_actions[i,:] = np.random.uniform(self._action_space_low, self._action_space_high)
        action_bounds = scipy.optimize.Bounds(np.tile(self._action_space_low,(self._horizon,)),
                                              np.tile(self._action_space_high, (self._horizon,)))
        print(initial_actions)
        mpc_solution = scipy.optimize.minimize(cost_at_horizon, initial_actions, args=state,
                                               bounds=action_bounds,tol=0.1, options={'maxiter':25})
        optimal_actions = np.reshape(mpc_solution.x, (self._horizon, self._action_dim))
        best_action = optimal_actions[0]
        return best_action

def gaussian_entropy(var):
    # for n independent gaussians, is this right?
    return np.sum(0.5*np.log(2*np.pi*np.exp(1)*var))
